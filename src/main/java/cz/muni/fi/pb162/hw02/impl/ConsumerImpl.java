package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.mesaging.broker.Broker;
import cz.muni.fi.pb162.hw02.mesaging.broker.Message;
import cz.muni.fi.pb162.hw02.mesaging.client.Consumer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

/**
 * Implementation class of Consumer interface.
 *
 * @author Adam Paulen
 */
public class ConsumerImpl extends ClientImpl implements Consumer {
    private final Map<String, Long> offsets = new HashMap<>();

    /**
     * creates a Customer.
     *
     * @param broker to be used
     */
    public ConsumerImpl(Broker broker) {
        super(broker);
    }

    @Override
    public Collection<Message> consume(int num, String... topics) {
        return consume(true, offsets, num, topics);
    }

    @Override
    public Collection<Message> consume(Map<String, Long> offsets, int num, String... topics) {
        return consume(false, offsets, num, topics);
    }

    /**
     * Implementation of consume Method
     *
     * @param update  switch for updating the offsets
     * @param offsets offsets to be used
     * @param num     number of messages from each topic
     * @param topics  topics to be read from
     * @return returns all Messages
     */

    private Collection<Message> consume(boolean update, Map<String, Long> offsets, int num, String... topics) {
        Collection<Message> topicMessages = broker.poll(offsets, num, List.of(topics));
        if (update) {
            updateOffsetIds(topicMessages, num, topics);
        }
        return topicMessages;
    }


    /**
     * Method to updates offsets of read messages
     *
     * @param messages List of messages
     * @param num      number of messages from each topic
     * @param topics   topics to be updated
     */
    private void updateOffsetIds(Collection<Message> messages, int num, String[] topics) {
        HashMap<String, ArrayList<Long>> idsMap = new HashMap<>();
        for (String topic : topics) {
            idsMap.put(topic, new ArrayList<Long>());
        }

        for (Message msg : messages) {
            for (String topic : msg.topics()) {
                ArrayList<Long> idsList = idsMap.get(topic);
                if (idsList != null) {
                    idsList.add(msg.id());
                }
            }
        }

        for (String topic : topics) {
            List<Long> list = idsMap.get(topic);
            if (!list.isEmpty()) {
                Collections.sort(list);
                Long id = list.size() < num ? list.get(list.size() - 1) : list.get(num - 1);
                offsets.put(topic, id);
            }
        }
    }

    @Override
    public Map<String, Long> getOffsets() {
        return new HashMap<>(offsets);
    }

    @Override
    public void setOffsets(Map<String, Long> offsets) {
        clearOffsets();
        updateOffsets(offsets);
    }

    @Override
    public void clearOffsets() {
        offsets.clear();
    }

    @Override
    public void updateOffsets(Map<String, Long> offsets) {
        for (String topic : offsets.keySet()) {
            this.offsets.put(topic, offsets.get(topic));
        }
    }
}
