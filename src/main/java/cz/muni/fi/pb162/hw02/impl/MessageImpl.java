package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.mesaging.broker.Message;

import java.util.Map;
import java.util.Set;

/**
 * Implementation of Message.
 *
 * @param id identification of Message
 * @param topics topics of Message
 * @param data data of Message
 * @author Adam Paulen
 */
public record MessageImpl(Long id, Set<String> topics, Map<String, Object> data) implements Message {
}
