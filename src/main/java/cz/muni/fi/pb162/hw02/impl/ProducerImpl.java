package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.mesaging.broker.Broker;
import cz.muni.fi.pb162.hw02.mesaging.broker.Message;
import cz.muni.fi.pb162.hw02.mesaging.client.Producer;

import java.util.Collection;
import java.util.Set;

/**
 * Implementation class of Client interface.
 *
 * @author Adam Paulen
 */
public class ProducerImpl extends ClientImpl implements Producer {

    /**
     * Creates a Producer
     *
     * @param broker broker to be used
     */
    public ProducerImpl(Broker broker) {
        super(broker);
    }

    @Override
    public Message produce(Message message) {
        Collection<Message> collMessage = broker.push(Set.of(message));
        return collMessage.iterator().next();
    }

    @Override
    public Collection<Message> produce(Collection<Message> messages) {
        return broker.push(messages);
    }
}
