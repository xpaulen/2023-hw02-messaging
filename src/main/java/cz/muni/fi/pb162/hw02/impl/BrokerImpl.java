package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.mesaging.broker.Broker;
import cz.muni.fi.pb162.hw02.mesaging.broker.Message;


import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Implementation class of Broker interface.
 *
 * @author Adam Paulen
 */
public class BrokerImpl implements Broker {
    private static final AtomicLong ID_COUNTER = new AtomicLong();
    private final Map<String, List<Message>> database = new HashMap<>();

    @Override
    public Collection<String> listTopics() {
        return database.keySet();
    }

    @Override
    public Collection<Message> push(Collection<Message> messages) {
        Set<Message> storedMessages = new HashSet<>();
        for (Message message : messages) {

            MessageImpl msg = new MessageImpl(ID_COUNTER.getAndIncrement(), message.topics(), message.data());
            for (String topic : msg.topics()) {
                List<Message> topicMessages = database.computeIfAbsent(topic, lambda -> new LinkedList<>());
                topicMessages.add(msg);
            }
            storedMessages.add(msg);
        }
        return storedMessages;
    }

    @Override
    public Collection<Message> poll(Map<String, Long> offsets, int num, Collection<String> topics) {
        ArrayList<Message> messages = new ArrayList<>();
        for (String topic : topics) {
            LinkedList<Message> topicMessages = (LinkedList<Message>) database.get(topic);
            if (topicMessages == null){
                continue;
            }
            long offset = offsets.getOrDefault(topic, (long) -1);
            int counter = 0;
            for (Message msg : topicMessages) {
                if (counter < num && offset < msg.id() && !messages.contains(msg)) {
                    messages.add(msg);
                    counter++;
                }
            }
        }
        return messages;
    }
}
